import 'package:flutter/material.dart';
import 'package:flutter_app_sample/bloc/cart_bloc.dart';
import 'package:flutter_app_sample/model/item.dart';
import 'package:flutter_app_sample/model/widgets/cart_action.dart';
import 'package:flutter_app_sample/model/widgets/item_list_item.dart';
import 'package:provider/provider.dart';

class ShopPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final items = Item.createSampleItems(20);
    final provider = Provider.of<CartBloc>(context);

    return Scaffold(
      appBar: AppBar(
        title: Text('Shop'),
        centerTitle: true,
        actions: <Widget>[
          CartAction(
            onTap: () {},
          )
        ],
      ),
      body: Container(
        color: Colors.grey,
        child: ListView.builder(
            itemCount: items.length,
            itemBuilder: (context, index) {
              return ItemListItem(
                items[index],
                onAddPressed: () {
                  provider.updateItems(items[index], 1, true);
                },
              );
            }),
      ),
    );
  }
}
