class Item {
  int id;
  String itemName;
  String description;
  double price;

  /// create sample static items
  static List<Item> createSampleItems(int number) {
    List<Item> items = List();

    for (int i = 0; i < number; i++) {
      Item item = Item()
        ..id = i
        ..itemName = 'Item #${i + 1}'
        ..description =
            'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras id urna ornare, tempus nisi id, hendrerit ligula. Integer condimentum fringilla leo, suscipit porttitor risus. Nulla convallis blandit urna eu sagittis. In dolor nisl, iaculis vel rutrum nec, iaculis vel risus. Donec sit amet pulvinar eros. Cras consectetur purus et dui interdum vestibulum. Maecenas commodo venenatis turpis, non congue diam convallis auctor. In lobortis at nisl nec feugiat. Ut lacus nulla, euismod in sodales a, dapibus vel nunc. Cras placerat libero non sem fringilla, nec porttitor est tincidunt.'
        ..price = (i + 1) * 8.0;
      items.add(item);
    }
    return items;
  }
}

class CartItem {
  Item item;
  int quantity;
}
