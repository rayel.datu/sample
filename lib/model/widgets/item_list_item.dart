import 'package:flutter/material.dart';
import 'package:flutter_app_sample/bloc/cart_bloc.dart';
import 'package:flutter_app_sample/model/item.dart';
import 'package:provider/provider.dart';

class ItemListItem extends StatelessWidget {
  final Item item;
  final VoidCallback onAddPressed;

  ItemListItem(this.item, {@required this.onAddPressed});

  @override
  Widget build(BuildContext context) {
    final provider = Provider.of<CartBloc>(context);
    return Card(
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Row(
          mainAxisSize: MainAxisSize.max,
          children: <Widget>[
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(
                    item.itemName,
                    style: TextStyle(fontSize: 18),
                    maxLines: 2,
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 8.0),
                    child: Text(
                      item.description,
                      softWrap: true,
                      textAlign: TextAlign.start,
                      maxLines: 2,
                      overflow: TextOverflow.ellipsis,
                      style: TextStyle(
                          fontStyle: FontStyle.italic, color: Colors.grey),
                    ),
                  ),
                ],
              ),
            ),
            Text('P${item.price.toStringAsFixed(2)}'),
            Column(
              children: <Widget>[
                IconButton(
                  icon: Icon(Icons.add),
                  onPressed: onAddPressed,
                ),
                provider.item(item) == null
                    ? Container()
                    : Container(
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.all(Radius.circular(4)),
                            color: Colors.red),
                        child:
                            Text('In Cart: ${provider.item(item).quantity}'),
                      )
              ],
            ),
          ],
        ),
      ),
    );
  }
}
