import 'package:flutter/material.dart';
import 'package:flutter_app_sample/bloc/cart_bloc.dart';
import 'package:provider/provider.dart';

class CartAction extends StatelessWidget {
  final VoidCallback onTap;

  CartAction({@required this.onTap});

  @override
  Widget build(BuildContext context) {
    final provider = Provider.of<CartBloc>(context);
    return InkWell(
      onTap: onTap,
      child: Container(
        decoration: BoxDecoration(shape: BoxShape.circle),
        width: 40,
        child: Stack(
          children: <Widget>[
            Center(
              child: Icon(
                Icons.shopping_cart,
                color: Colors.white,
              ),
            ),
            provider.cartItemSize() != 0
                ? Container(
                    padding: EdgeInsets.all(6),
                    decoration: BoxDecoration(
                      shape: BoxShape.circle,
                      color: Colors.red,
                    ),
                    child: Text(
                      provider.cartItemSize().toString(),
                      style: TextStyle(color: Colors.white),
                    ),
                  )
                : Container()
          ],
        ),
      ),
    );
  }
}
