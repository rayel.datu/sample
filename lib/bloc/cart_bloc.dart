import 'package:flutter/cupertino.dart';
import 'package:flutter_app_sample/model/item.dart';

class CartBloc with ChangeNotifier {
  List<CartItem> _cartItems;

  /// create an empty list every time bloc is initialized
  CartBloc() {
    _cartItems = List();
  }

  /// add item to cartItems or update the quantity if it already exists.
  updateItems(Item itemToAdd, int quantity, bool increment) {
    if (_cartItems.isEmpty) {
      _cartItems.add(CartItem()
        ..item = itemToAdd
        ..quantity = 1);
      notifyListeners();
      return;
    }

    CartItem existingItem =
        _cartItems.firstWhere((item) => item.item.id == itemToAdd.id);

    if (existingItem != null) {
      if (increment) {
        _cartItems.firstWhere((item) {
          item.quantity += quantity;
          return true;
        });
      } else {
        _cartItems.firstWhere((item) {
          item.quantity = quantity;
          return true;
        });
      }
    } else {
      _cartItems.add(CartItem()
        ..item = itemToAdd
        ..quantity = 1);
    }
    notifyListeners();
  }

  /// remove item from cart
  removeItem(Item itemToRemove) {
    _cartItems.removeWhere((item) => item.item.id == itemToRemove.id);
    notifyListeners();
  }

  /// get cart items size
  int cartItemSize() => _cartItems.length;

  CartItem item(Item item) => _cartItems.isNotEmpty
      ? _cartItems.contains(item)
          ? _cartItems.firstWhere((e) => e.item.id == item.id)
          : null
      : null;
}
