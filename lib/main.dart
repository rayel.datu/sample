import 'package:flutter/material.dart';
import 'package:flutter_app_sample/bloc/cart_bloc.dart';
import 'package:flutter_app_sample/model/shop.dart';
import 'package:provider/provider.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider<CartBloc>(
          create: (_) => CartBloc(),
        )
      ],
      child: MaterialApp(
        title: 'Flutter Demo',
        theme: ThemeData(
          primarySwatch: Colors.blue,
        ),
        home: ShopPage(),
      ),
    );
  }
}
